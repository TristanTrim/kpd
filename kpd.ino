/*

Its a robot kite.

*/

// CONTROLLER DATAS/ old    //  current
#define WIRE_1 10 // red    //  RED
#define WIRE_2 9  // yellow //  WHITE
#define WIRE_3 8  // green  //  BLACK
int button_event();
int instant_button_state();
int cleaned_button_state();
char const * command_name();
#define ALL_RELEASED 0x20
#define MENU_BUTTON 0x10
#define BUTTON_1 0x1
#define BUTTON_2 0x2
#define BUTTON_3 0x4
#define BUTTON_4 0x8

// DC MOTOR DATAS
//#define NEW_ESC  //comment / uncomment to switch between new and old esc settings
#ifdef NEW_ESC
  int motor_pin = 11;
  int motor_null = 12; // Switching to new ESC 
  int reel_in_speed = 3; // https://reference.digilentinc.com/dmc-60/reference-manual
  int reel_out_speed = 3;
  int dc_mot_max = 4;
  int dc_mot_min = 1;
#else
  int motor_pin = 11;
  int motor_null = 187; // was 191 as per measured, but 187 works better with arduino signal.
  int reel_in_speed = 30;
  int reel_out_speed = 10;
  int dc_mot_max = 60;
  int dc_mot_min = 7;
#endif




// STEPPER MOTOR DATAS
int step_step_pin = 6;
int step_dir_pin = 7;

 // HIGHER NUMBER MEANS SLOWER SPEED
//int step_pulse_delay = 2; // 2 is fastest <2 breaks
int step_out_delay = 2;
int step_in_delay = 2;

// FLIGHT STATE DATAS
unsigned int flight_count = 0;
bool input_changed = false;
bool reeling = false, reel_dir = false; // false in, true out
bool AOAing = false, AOA_dir = false;   // false down, true up
void kite_input(int);

// Demo DATAS
int flight_mode = 0; // 0: manual, 1: automatic
int demo_state = 0;
unsigned int demo_count = 0;
int AOA_time = 50;
int reel_in_time = 30;
int reel_out_time = 30;
int ESC_wake_time = 10;
int demo_keep_reeling = 0;

// Dummy vs ESC
int conn_to_esc = 1; // 0: connected to dummy load, 1: connected to ESC
int conn_to_esc_pin = 13;

// MENU DATAS
bool in_menu = false;
int menu_cursor = 1;

struct menu_item { char const text[13]; int * const value; int const minum; int const maxim;};
int const menu_item_num = 12;
int const dummy = 7777;
struct menu_item menu[menu_item_num + 2] = {
  {"============",         &dummy,     0,            0},
  {"Dummy / ESC ",   &conn_to_esc,     0,            1},
  {"Flight mode ",   &flight_mode,     0,            1},
  {"D: AOA time ",      &AOA_time,     0,          100},
  {"D: rl in tm ",  &reel_in_time,     0,          100},
  {"D: rl out tm", &reel_out_time,     0,          100},
  {"D: ESC wk*10", &ESC_wake_time,     0,          100},
  {"D: kp rling ", &demo_keep_reeling, 0,            1},
  {"Reel in spd ", &reel_in_speed,     dc_mot_min, dc_mot_max},
  {"Reel out spd", &reel_out_speed,    dc_mot_min, dc_mot_max},
  {"Reel offset ",    &motor_null,     1,          255},
  {"AOA up spd  ",&step_out_delay,     2,          100},
  {"AOA down spd", &step_in_delay,     2,          100},
  {"============",         &dummy,     0,            0}
};
void update_display();
void menu_function (int);

// DISPLAY DATA
#include "Wire.h"
#include "Adafruit_LiquidCrystal.h"
Adafruit_LiquidCrystal lcd(0);
bool redraw_display = true;

// SETUP //
void setup()
{
Serial.begin(9600);
// DC MOTOR SETUP
  #ifdef NEW_ESC
    TCCR2B = (TCCR2B & 0b11111000) | 0x07;  // slow down the pwm on pins 11 and 12
  #endif
  pinMode(motor_pin, OUTPUT);
  pinMode(conn_to_esc, OUTPUT);
  
  // DISPLAY SETUP
  lcd.begin(16,4);
}

///////////////
// MAIN LOOP //
///////////////
void loop()
{
//// Update input from controller ////
int button_state = button_event();
if (button_state == MENU_BUTTON) {
  in_menu = !in_menu;
    redraw_display = true;
  }
if (in_menu) {
  if (reeling || AOAing){
    reeling = false;
    AOAing = false;
    input_changed = true;
  }
  menu_function(button_state);
} else {
  if (flight_mode == 1){
    // kite in demo mode
    demo_count++;
    if((demo_state==0) // INITIAL
     ||((demo_count>( ESC_wake_time *100))&&(demo_state==5)) ) {
      demo_count=0;
      demo_state=1;
      conn_to_esc = 1;
      kite_input(BUTTON_3); // AOAing down
      input_changed = true;
    } else if ((demo_count>( AOA_time *1000))&&(demo_state==1)){
      demo_state=2;
      demo_count=0;
      kite_input(BUTTON_2); // reeling in
      input_changed = true;
    } else if ((demo_count>( reel_in_time *1000))&&(demo_state==2)) {
      demo_state=3;
      demo_count=0;
      if (demo_keep_reeling){
        kite_input(BUTTON_2 | BUTTON_4); // AOAing up while holding reel in.
      }else{
        kite_input(BUTTON_4); // AOAing up without reeling.
      }
      input_changed = true;
    }else if ((demo_count>( AOA_time *1000))&&(demo_state==3)) {
      demo_state=4;
      demo_count=0;
      conn_to_esc = 0;
      kite_input(ALL_RELEASED); // releas all to let reel out.
      input_changed = true;
    }else if ((demo_count>( reel_out_time *1000))&&(demo_state==4)) {
      demo_state=5;
      demo_count=0;
      conn_to_esc = 1;
      kite_input(BUTTON_1); // reel out to wake up ESC.
      input_changed = true;
    }
  } else {
    demo_state=0;

    // human input
    if (button_state) {
      // kite under human control
      input_changed = true;
      kite_input(button_state);
    } else { input_changed = false; }
  }
}
update_kite_control();
update_display();
}


// MAIN CODE FUNCTIONS

void kite_input(int user_input)
{
  if (user_input & BUTTON_1) {reeling=true; reel_dir = true;}
  else if (user_input & BUTTON_2) {reeling=true; reel_dir = false;}
  else {reeling = false;}
  if (user_input & BUTTON_3) {AOAing =true; AOA_dir = false;}
  else if (user_input & BUTTON_4) {AOAing =true; AOA_dir = true;}
  else {AOAing = false;}
}

// update_kite_control checks input_changed
// which should be set to indicate that some change
// in control needs to be written to the pinouts.
// update_kite_control then converts from the
// human readable control schema to the pinouts.
void update_kite_control() {
  flight_count++;
  // Dummy vs ESC update
  digitalWrite(conn_to_esc_pin, conn_to_esc);
  // DC MOTOR UPDATE
  if (input_changed) {
    int motor_signal = motor_null;
    if (reeling) { //indicates the direction of rotation for the DC Motor
      if (reel_dir) {motor_signal += reel_out_speed;} // reeling out
      else {motor_signal -= reel_in_speed;} // reeling in
      }
    analogWrite(motor_pin, motor_signal);
  }
  // STEPPER MOTOR UPDATE
  if (AOAing){  //indicates direction of rotation for the stepper 
    if (input_changed) digitalWrite(step_dir_pin, AOA_dir);
    int step_mod;
    if (AOA_dir){
            step_mod = flight_count%step_in_delay;
    }else { step_mod = flight_count%step_out_delay;} 

    if (step_mod == 0){
      digitalWrite(step_step_pin, HIGH);
    }else {
      digitalWrite(step_step_pin, LOW);
    }
  }
}

// DC MOTOR FUNCTIONS

// CONTROLLER CONTROL FUNCTIONS

char const * command_name(int state)
{
  switch (state)
  {
  case 0:
  return NULL;
  case 0x01:
  return "Altitude Up    Attack ~~~ ";
  case 0x02:
  return "Altitude Down  Attack ~~~ ";
  case 0x04:
  return "Altitude ~~~   Attack Up";
  case 0x05:
  return "Altitude Up    Attack Up";
  case 0x06:
  return "Altitude Down  Attack Up";
  case 0x08:
  return "Altitude ~~~   Attack Down";
  case 0x09:
  return "Altitude Up    Attack Down";
  case 0x0a:
  return "Altitude Down  Attack Down";
  case 0x10:
  return "Menu";
  case 0x20:
  return "Altitude ~~~   Attack ~~~ ";
  default:
  Serial.println(state, BIN);
  return "she's fubar'd!";
  }
}

// button_event returns button presses only when the button changes.
int button_event() // 0 unchanged, 0x20 returned to zero, anything else, controls.
{
static int prev_event_state = 0;
int event_state = cleaned_state();
if (event_state != prev_event_state){
  prev_event_state = event_state;
  if (event_state) return event_state;
  return 0x20;
  }
return 0;
}

// cleaned_state if for removing nonsense inputs from the controller
// and for debouncing it.
int cleaned_state()
{
static int current_state = 0;
static int change_buf = 0;
int state = instant_button_state();
  if (state >= 0x10) state = 0x10; // Any button with the menu button is just the menu button. There's ghosting anyway.
  if ((state != current_state) && ((state & 0x3)!=0x3) && ((state & 0xc)!=0xc)) // button state changed, and not to having up & down pressed.
  change_buf++;
  else
  change_buf=0;
  if ( change_buf > 5 ){
  current_state = state;
  }
return current_state;
}

int instant_button_state()
{
// initialize the byte that will hold the information about our keystates
int state = 0xffe0;

//  Button 1
pinMode(WIRE_3, INPUT);
pinMode(WIRE_1, INPUT_PULLUP);
pinMode(WIRE_2, OUTPUT);
digitalWrite(WIRE_2,LOW);
state |= digitalRead(WIRE_1);

//  Button 2
pinMode(WIRE_2, INPUT);
pinMode(WIRE_1, INPUT_PULLUP);
pinMode(WIRE_3, OUTPUT);
digitalWrite(WIRE_3,LOW);
state |= digitalRead(WIRE_1) << 1;

//  Button 3
pinMode(WIRE_2, INPUT);
pinMode(WIRE_3, INPUT_PULLUP);
pinMode(WIRE_1, OUTPUT);
digitalWrite(WIRE_1,LOW);
state |= digitalRead(WIRE_3) << 2;

//  Button 4
pinMode(WIRE_3, INPUT);
pinMode(WIRE_2, INPUT_PULLUP);
pinMode(WIRE_1, OUTPUT);
digitalWrite(WIRE_1,LOW);
state |= digitalRead(WIRE_2) << 3;

//  Button 5
pinMode(WIRE_1, INPUT);
pinMode(WIRE_2, INPUT_PULLUP);
pinMode(WIRE_3, OUTPUT);
digitalWrite(WIRE_3,LOW);
bool button_5 = digitalRead(WIRE_2);
pinMode(WIRE_3, INPUT_PULLUP);
pinMode(WIRE_2, OUTPUT);
digitalWrite(WIRE_2,LOW);
state |= (button_5 || digitalRead(WIRE_3)) << 4;

return ~state;
}

// MENU FUNCTIONS

void menu_function (int button_state){
  switch (button_state){
  case 0:
    break;
  case BUTTON_1:
    menu_cursor--;
    if (menu_cursor < 1) menu_cursor = 1;
    else redraw_display = true;
    break;
  case BUTTON_2:
    menu_cursor++;
    if (menu_cursor > menu_item_num) menu_cursor = menu_item_num;
    else redraw_display = true;
    break;
  case BUTTON_3:
    {
    struct menu_item * item = &menu[menu_cursor];
    (*item->value)++;
    if (*item->value > item->maxim) *item->value = item->maxim;
    else redraw_display = true;
    }
    break;
  case BUTTON_4:
    {
    struct menu_item * item = &menu[menu_cursor];
    (*item->value)--;
    if (*item->value < item->minum) *item->value = item->minum;
    else redraw_display = true;
    }
    break;
  case ALL_RELEASED:
  case MENU_BUTTON:
    break;
  default:
    Serial.println("Something in the menu_function broke");
  }
}


// DISPLAY FUNCTIONS

void update_display() {
  if (redraw_display) {
    redraw_display = false; // we're doin' it!
    lcd.clear();
    lcd.print("NSCC EETG KPD");
    if (in_menu) {
      char buf[17];
      // write third row of menu
      lcd.setCursor(-4,3);
      sprintf(buf,"%s%4d", menu[menu_cursor+1].text, *menu[menu_cursor+1].value);
      lcd.print(buf);
      // write second row of menu
      lcd.setCursor(-4,2);
      sprintf(buf,"%s%4d", menu[menu_cursor].text, *menu[menu_cursor].value);
      lcd.print(buf);
      // write first row of menu
      lcd.setCursor(0,1);
      sprintf(buf,"%s%4d", menu[menu_cursor-1].text, *menu[menu_cursor-1].value);
      lcd.print(buf);
    }
  }
}